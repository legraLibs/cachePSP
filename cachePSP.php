<?php
//if (DEV_LVL>2) echo __FILE__.'<br>';
/* cachePSP.php
 * creait un fichier de cache par psp complet
 *
 */
//if (DEV_LVL>2) echo __FILE__.'<br>';
define ('CACHEPSP_VERSION','0.0.2-dev');
$gestLib->loadLib('CachePSP',__FILE__,CACHEPSP_VERSION,'gestionnaire de cache selon le PSP');

class CachePSP{
    private $level;
    private $engine;
    private $cacheRefresh;
    private $pause;// ne pas utiliser le cache temporairement meme si l'engine est on

    private $cacheRoot;
    private $cacheFile;
    private $cachePath; // calc

    private $isClearCache;

    private $utilisateur; // si defini les fichiers cachent seront dans un repertoire au nom de l'utilisateur (definir apres cacheRoot

    function __construct(){
        $this->cacheFile=$_SERVER['QUERY_STRING'];
        $this->cacheRoot='./cache';
        $this->level=1;// On
        $this->engine=1;
        $this->refresh=0;
        $this->pause=0;

        $this->isClearCache=0;

        $this->utilisateur='';

        // - init par le formulaire - //
        if (isset($_POST['MVC_fmenuSelect'])){
            $this->setEngine (isset($_POST['cache_engine' ])?1:0);
            $this->setRefresh(isset($_POST['cache_refresh'])?1:0);
            $this->setPause(isset($_POST['cache_pause'])?1:0);

        }
        else{ // optionnel
            if (isset($_GET['cache_engine'])){$this->setEngine($_GET['cache_engine']);}
            elseif (isset($_SESSION['cache_engine'])){$this->setEngine($_SESSION['cache_engine']);}

            
            if (isset($_GET['cache_refresh'])){$this->setRefresh($_GET['cache_refresh']);}
            elseif (isset($_SESSION['cache_refresh'])){$this->setRefresh($_SESSION['cache_refresh']);}
        }
        //$this->setCacheRoot();//doit etre appeller par le client

        //$this->clearCache();

    }

    function __toString(){
        return array(
             'level'=>$this->level
            ,'engine'=>$this->engine
            ,'refresh'=>$this->refresh
            ,'pause'=>$this->pause
            ,'cacheRoot'=>$this->cacheRoot
            ,'cacheFile'=>$this->cacheFile
            ,'cachePath'=>$this->cachePath
            ,'isClearCache'=>$this->isClearCache
            ,'utilisateur'=>$this->utilisateur
            );
    }


    // - hydratation - //
    function setEngine($val=1){
        $this->engine=(int)$val;
        $_SESSION['cache_engine']=$this->engine;
    }
    function setRefresh($val=1){
        $this->refresh=(int)$val;
        $_SESSION['cache_refresh']=$this->refresh;
    }
    function setPause($val=1){
        $this->pause=(int)$val;
    }

    // - definir APRES cacheROOT - //
    function setUtilisateur($user,$cacheRoot=''){
        $this->utilisateur=str_replace('/','',$user);
        $this->setCacheRoot($cacheRoot===''?$this->cacheRoot.$this->utilisateur:$cacheRoot.$this->utilisateur);
    }

    function setCacheRoot($dir=NULL){
        if ($dir===NULL) $dir=$this->cacheRoot;

        //echo gestLib_inspect('$this->cacheFile'.__LINE__,$this->cacheFile);
        // - gestion d'absence d'URI - //
        if ($this->cacheFile==''){
            //echo 'PAS DE URI';//test
            $this->cacheFile='default';
        }
        $this->cachePath=NULL;

        
        if ( $dir[strlen($dir)-1] !== DIRECTORY_SEPARATOR) $dir.=DIRECTORY_SEPARATOR;

        if (file_exists($dir)){
            $this->cacheRoot=$dir;
            $this->cachePath=$this->cacheRoot.$this->cacheFile.'.cache';
            return 0;
        }
        else{
            if(mkdir($dir,0777,$recursive=TRUE) === TRUE){ //TRUE/FALSE
                $this->cacheRoot=$dir;
                $this->cachePath=$this->cacheRoot.$this->cacheFile.'.cache';
                return 0;
            }
            $this->engine=0;// desactivation du systeme de cache
            echo 'Erreur lors de la creation du rep:'.$dir;
            return 1; //erreur
        }
    }

    // - - //
    function clearCache(){
        if ($this->isClearCache===1)return rmdir ($this->cachePath);
        return 0;
    }


    // - - //ul c aussi intense
    function saveCache(){
        if ($this->engine===0 OR $this->pause===1)return 0;
        if (!file_exists($this->cachePath) OR $this->refresh===1){
            $dateLastCache=date('d/m/Y H:i');
 
             // - code js - //
            $js='<script>/*ajouter par saveCache*/';
            $js.='poId=document.getElementById("pageInfos");';
            $js.='if (poId !== undefined){';
            $js.='poId.innerHTML ="cache du '.$dateLastCache.'.<br>";';
            $js.='poId.innerHTML+=" Fichier:<i>'.$this->cacheFile.'</i>";';
            $js.='}</script>'."\n";

            file_put_contents($this->cachePath,ob_get_contents().$js,LOCK_EX);
            // gestion d erreur en cas d'impossibilité de creation/ecriture du fichier (dir inexistant?)
        }
    }
    // - - //
    function showCache(){
        if ($this->engine===0 OR $this->refresh===1)return 0;
        if ($this->pause===1) return 0;
        if (file_exists($this->cachePath)){
            //if (DEV_LVL >=0) echo __FILE__.__LINE__.':le cache '.$this->cachePath.' existe';
            readfile($this->cachePath);
            return 1;
        }
    return 0;
    }


    // - formulaire - //
    function selectLevel(){
    $o='';
    $checked=$this->engine===1?' checked':'';
    $o.='<span class="labelChamp"><input type="checkbox" name="cache_engine"  value="1"'.$checked.'><label>engine</label></span>';
    $checked=$this->refresh===1?' checked':'';
    $o.=' <span class="labelChamp"><input type="checkbox" name="cache_refresh" value="1"'.$checked.'><label>refresh</label></span>';
    //$o.='<input type="checkbox" name="cache_clear"   value="'.$this->clear.'"><label>clear</label><br>';
    return '<div>'.$o.'</div>';
    }
    
} // CachePSP



$gestLib->libs['CachePSP']->git='';
$gestLib->end('CachePSP');
